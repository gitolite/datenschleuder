# License: GNU Free Documentation License (GFDL) http://www.gnu.org/copyleft/fdl.html

LATEX=latex
DVIPDF=dvipdf
PDFLATEX=pdflatex
TOCCONVERTER=openfm/bin/convert-toc-10-16.sh # use this for hex-pagenumbers

TARGETS := main

all: clean $(TARGETS)

$(TARGETS):
	@echo "[PDFLATEX]\r\t\t\b\b\b$@.tex"
	@$(PDFLATEX) $@.tex >/dev/null
	@echo "[TOCHEX]\r\t\t\b\b\b$@.toc"
	@$(TOCCONVERTER) $@.toc
	@echo "[PDFLATEX]\r\t\t\b\b\b$@.tex"
	@$(PDFLATEX) $@.tex >/dev/null
clean:
	@echo [CLEAN]
	@find . -maxdepth 1 -mindepth 1 -type f -not -name "*.tex" -not -name "*.sty" -not -name "Makefile" -not -name "*.dtx" -not -name "*.drv" | xargs rm -f
