# License: GNU Free Documentation License (GFDL) http://www.gnu.org/copyleft/fdl.html
#!/bin/sh

TOCFILE=$1

[ -f "$TOCFILE" ] || exit 0

if [ ! -f "$BC_ENV_ARGS" ]; then
  export BC_ENV_ARGS=""
fi

cat $TOCFILE | while read line; do
  PAGENUMBER_DEC="$(echo $line | rev | cut -f3 -d"}" | rev |cut -b2-)"
  PAGENUMBER_HEX="$(echo "obase=16; ibase=10; $PAGENUMBER_DEC" | bc)"
  PAGENUMBER_HEX_SIZE="$(echo $PAGENUMBER_HEX | wc -c)"
  FORMATTED_PAGE_NUMBER="0x"
  [ $PAGENUMBER_HEX_SIZE -lt 3 ] && FORMATTED_PAGE_NUMBER="${FORMATTED_PAGE_NUMBER}0"
  FORMATTED_PAGE_NUMBER="${FORMATTED_PAGE_NUMBER}${PAGENUMBER_HEX}"
  THELINE="$(echo $line | rev | cut -f-2 -d"}")}$(echo "$FORMATTED_PAGE_NUMBER" |rev){}$(echo $line | rev | cut -f4- -d"}")\\"
  echo $THELINE |rev >> $1.tmp
done
mv $1.tmp $1
